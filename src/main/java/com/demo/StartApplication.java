package com.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName StartApplication
 * @Description TODO
 * @Author QuanHao
 * @Date 2021/12/5 21:03
 * @Version 1.0
 **/

@SpringBootApplication
public class StartApplication {
    public static void main(String[] args) {
        SpringApplication.run(StartApplication.class,args);
    }
}

package com.demo.aspect;

import com.alibaba.fastjson.JSON;
import com.demo.config.LogsProperties;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @ClassName LogAspect
 * @Description TODO
 * @Author QuanHao
 * @Date 2021/12/5 20:13
 * @Version 1.0
 **/
@Aspect
@Component
@Slf4j
public class ServiceLogAspect {
    @Autowired
    private LogsProperties properties;

    private Method method;
    private String className;

    @Pointcut("execution(* com.*.service.impl..*.*(..))")
    public void serviceLog(){

    }
    @Before("serviceLog()")
    public void before(JoinPoint joinPoint){
        this.getData(joinPoint);
        log.info("开始调用service[{}]的[{}]方法",className,method);
        //方法参数的类型
//        Class<?>[] parameterTypes = method.getParameterTypes();
//        for (Class<?> clas : parameterTypes) {
//            String parameterName = clas.getName();
//            System.out.println("参数类型:" + parameterName);
//
//        }
        Map<String, Object> map = new HashMap<String, Object>();
        //参数名称
        String[] names=((MethodSignature) joinPoint.getSignature()).getParameterNames();
        //参数值
        Object[] objects=joinPoint.getArgs();
        for (int i = 0; i < names.length; i++) {
            map.put(names[i], objects[i]);
        }
        if (properties.isServiceLog()){
            log.info("调用[{}]方法的参数及值为[{}]",method, JSON.toJSONString(names),JSON.toJSONString(map));
        }
    }

    @AfterReturning(pointcut = "serviceLog()",returning = "result")
    public void after(JoinPoint joinPoint,Object result){
        this.getData(joinPoint);
        if (properties.isServiceLog()) {
            log.info("调用成功[{}]的[{}]方法，返回参数为[{}]",className,method,JSON.toJSONString(result));
        }else log.info("调用成功[{}]的[{}]方法",className,method);
    }

    @AfterThrowing(pointcut = "serviceLog()",throwing = "ex")
    public void returnException(JoinPoint joinPoint,Exception ex) {
        this.getData(joinPoint);
        log.error("执行service[{}]的[{}]方法发生异常,异常信息[{}]",className,method,JSON.toJSONString(ex),ex);
    }

    private void getData(JoinPoint joinPoint){
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        //方法所属类的类名
        className =  methodSignature.getDeclaringTypeName();
        //获取当前切点方法对象
        method = methodSignature.getMethod();
    }
}

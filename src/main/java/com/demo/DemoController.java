package com.demo;

import com.demo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName DemoController
 * @Description TODO
 * @Author QuanHao
 * @Date 2021/12/5 20:21
 * @Version 1.0
 **/
@RestController
public class DemoController {

    @Autowired
    TestService testService;

    @GetMapping("/map")
    public void teset(String number){
        String formate = testService.formate(number);
    }
}

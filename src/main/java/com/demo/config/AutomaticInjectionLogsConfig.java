package com.demo.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName AutomaticInjectionLogsConfig
 * @Description TODO
 * @Author QuanHao
 * @Date 2021/12/5 20:06
 * @Version 1.0
 **/
@Configuration
@EnableConfigurationProperties({LogsProperties.class})
public class AutomaticInjectionLogsConfig {
}

package com.demo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @ClassName LogsProperties
 * @Description TODO
 * @Author QuanHao
 * @Date 2021/12/5 20:35
 * @Version 1.0
 **/
@ConfigurationProperties(prefix = "spring.logs")
@Data
public class LogsProperties {
    /**
     * service日志
     */
    private boolean serviceLog = false;
    /**
     * dao 日子
     */
    private boolean daoLog = false;

}
